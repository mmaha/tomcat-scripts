#!/usr/bin/env sh
TOMCAT_BASE=`ps -ef | ruby -ne 'puts $1 if $_ =~ /catalina.home=(\S*)/' | grep tomcat`
tail -f $TOMCAT_BASE/logs/catalina.out
