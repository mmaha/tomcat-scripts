#!/usr/bin/env sh

TOMCAT_HOST="localhost:8080"

if [ "$1" != "" ] && [ "$2" != "" ]; then
    curl http://$1:$2@$TOMCAT_HOST/manager/list
else
    curl http://tomcat:password@$TOMCAT_HOST/manager/list
fi
